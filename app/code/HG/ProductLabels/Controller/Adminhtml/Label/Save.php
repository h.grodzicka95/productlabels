<?php

namespace HG\ProductLabels\Controller\Adminhtml\Label;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Backend\App\Action\Context;
use HG\ProductLabels\Api\LabelRepositoryInterface;
use HG\ProductLabels\Model\LabelFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;

/**
 * Save Label action.
 */
class Save extends \HG\ProductLabels\Controller\Adminhtml\Label implements HttpPostActionInterface
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var LabelFactory|mixed
     */
    private $labelFactory;

    /**
     * @var LabelRepositoryInterface|mixed
     */
    private $labelRepository;

    /**
     * Save constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     * @param LabelFactory|null $labelFactory
     * @param LabelRepositoryInterface|null $labelRepository
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        DataPersistorInterface $dataPersistor,
        LabelFactory $labelFactory = null,
        LabelRepositoryInterface $labelRepository = null
    )
    {
        $this->dataPersistor = $dataPersistor;
        $this->labelFactory = $labelFactory
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(LabelFactory::class);
        $this->labelRepository = $labelRepository
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(LabelRepositoryInterface::class);
        parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            if (empty($data['label_id'])) {
                $data['label_id'] = null;
            }

            /** @var \HG\ProductLabels\Model\Label $model */
            $model = $this->labelFactory->create();
            $id = $data['label_id'];
            if ($id) {
                try {
                    $model = $this->labelRepository->getById($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This label no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            if ($id) {
                try {
                    $model = $model->load($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This label no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }
            $model->setData($data);

            if (isset($data['label_products'])
                && is_string($data['label_products'])) {
                $products = json_decode($data['label_products'], true);
                $model->setPostedProducts($products);
            }
            $this->_eventManager->dispatch(
                'hg_productlabels_prepare_save',
                ['label' => $model, 'request' => $this->getRequest()]
            );
            try {
                $this->labelRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the label.'));
                $this->dataPersistor->clear('labels');
                return $this->processBlockReturn($model, $data, $resultRedirect);
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the label.'));
            }

            $this->dataPersistor->set('labels', $data);
            return $resultRedirect->setPath('*/*/edit', ['label_id' => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    private function processBlockReturn($model, $data, $resultRedirect)
    {
        $redirect = $data['back'] ?? 'close';
        if ($redirect === 'continue') {
            $resultRedirect->setPath('*/*/edit', ['label_id' => $model->getId()]);
        } else if ($redirect === 'close') {
            $resultRedirect->setPath('*/*/');
        } else if ($redirect === 'duplicate') {
            $duplicateModel = $this->labelFactory->create(['data' => $data]);
            $duplicateModel->setId(null);
            $this->labelRepository->save($duplicateModel);
            $id = $duplicateModel->getId();
            $this->messageManager->addSuccessMessage(__('You duplicated the label.'));
            $this->dataPersistor->set('labels', $data);
            $resultRedirect->setPath('*/*/edit', ['label_id' => $id]);
        }
        return $resultRedirect;
    }

    /**
     * @inheritDoc
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('HG_ProductLabels::label_save');
    }
}
