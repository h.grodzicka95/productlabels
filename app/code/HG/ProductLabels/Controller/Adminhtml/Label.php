<?php

namespace HG\ProductLabels\Controller\Adminhtml;

use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\Store;
use Magento\Framework\Controller\ResultFactory;

abstract class Label extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'HG_ProductLabels::labels';

    /**
     * @var \Magento\Framework\Stdlib\DateTime\Filter\Date
     */
    protected $dateFilter;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    private $wysiwigConfig;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    private $authSession;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\Filter\Date|null $dateFilter
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwigConfig
     * @param \Magento\Backend\Model\Auth\Session $authSession
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Stdlib\DateTime\Filter\Date $dateFilter = null,
        \Magento\Store\Model\StoreManagerInterface $storeManager = null,
        \Magento\Framework\Registry $registry = null,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwigConfig = null,
        \Magento\Backend\Model\Auth\Session $authSession = null
    ) {
        $this->dateFilter = $dateFilter;
        $this->storeManager = $storeManager ?: ObjectManager::getInstance()->get(
            \Magento\Store\Model\StoreManagerInterface::class
        );
        $this->registry = $registry ?: ObjectManager::getInstance()->get(
            \Magento\Framework\Registry::class
        );
        $this->wysiwigConfig = $wysiwigConfig ?: ObjectManager::getInstance()->get(
            \Magento\Cms\Model\Wysiwyg\Config::class
        );
        $this->authSession = $authSession ?: ObjectManager::getInstance()->get(
            \Magento\Backend\Model\Auth\Session::class
        );
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function initPage($resultPage)
    {
        $resultPage->setActiveMenu('HG_ProductLabels::productlabels')
            ->addBreadcrumb(__('Product Labels'), __('Product Labels'));
        return $resultPage;
    }


    /**
     * Initialize requested label and put it into registry.
     *
     * @param bool $getRootInstead
     * @return \HG\ProductLabels\Model\Label|false
     */
    protected function _initLabel()
    {
        $labelId = $this->resolveLabelId();
        $storeId = $this->resolveStoreId();
        $label = $this->_objectManager->create(\HG\ProductLabels\Model\Label::class);

        if ($labelId) {
            $label->load($labelId);
        }

        $this->registry->unregister('label');
        $this->registry->unregister('current_label');
        $this->registry->register('label', $label);
        $this->registry->register('current_label', $label);
        $this->wysiwigConfig->setStoreId($storeId);
        return $label;
    }

    /**
     * Resolve Label Id (from get or from post)
     *
     * @return int
     */
    private function resolveLabelId() : int
    {
        $labelId = (int)$this->getRequest()->getParam('label_id', false);
        return $labelId ?: (int)$this->getRequest()->getParam('label_id', false);
    }
    /**
     * Resolve store id
     *
     * Tries to take store id from store HTTP parameter
     * @see Store
     *
     * @return int
     */
    private function resolveStoreId() : int
    {
        $storeId = (int)$this->getRequest()->getParam('store', false);
        return $storeId ?: (int)$this->getRequest()->getParam('store_id', Store::DEFAULT_STORE_ID);
    }
}
