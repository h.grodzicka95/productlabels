<?php

namespace HG\ProductLabels\Api;

use Magento\Catalog\Model\Product;

interface LabelRepositoryInterface
{
    /**
     * Save label.
     *
     * @param \HG\ProductLabels\Api\Data\LabelInterface $label
     * @return \HG\ProductLabels\Api\Data\LabelInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\LabelInterface $label): Data\LabelInterface;

    /**
     * Retrieve label.
     *
     * @param $labelId
     * @return \HG\ProductLabels\Api\Data\LabelInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($labelId): Data\LabelInterface;

    /**
     * Delete label.
     *
     * @param \HG\ProductLabels\Api\Data\LabelInterface $block
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\LabelInterface $label): bool;

    /**
     * Delete label by ID.
     *
     * @param  $labelId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($labelId): bool;

    /**
     * Get labels assigned to product
     * @param Product $product
     * @return \HG\ProductLabels\Api\Data\LabelInterface[]
     */
    public function getItemsByProduct($product);
}
