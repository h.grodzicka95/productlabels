<?php

namespace HG\ProductLabels\Api\Data;

interface LabelInterface
{
    const LABEL_ID = 'label_id';
    const LABEL_TEXT = 'label_text';
    const LABEL_COLOR = 'label_color';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getLabelId(): ?int;

    /**
     * Get label text
     *
     * @return string
     */
    public function getLabelText(): string;

    /**
     * Get label color
     *
     * @return string
     */
    public function getLabelColor(): string;

    /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * Get updated at
     *
     * @return string
     */
    public function getUpdatedAt(): string;

    /**
     * Set ID
     *
     * @param int $id
     * @return LabelInterface
     */
    public function setLabelId(int $id): LabelInterface;

    /**
     * Set Label Text
     *
     * @param string $labelText
     * @return LabelInterface
     */
    public function setLabelText(string $labelText): LabelInterface;

    /**
     * Set label color
     *
     * @param string $labelColor
     * @return LabelInterface
     */
    public function setLabelColor(string $labelColor): LabelInterface;

    /**
     * Set creation time
     *
     * @param string $createdAt
     * @return LabelInterface
     */
    public function setCreatedAt(string $createdAt): LabelInterface;

    /**
     * Set update time
     *
     * @param string $updatedAt
     * @return LabelInterface
     */
    public function setUpdatedAt(string $updatedAt): LabelInterface;
}
