<?php

namespace HG\ProductLabels\Api\Data;

interface LabelLinkManagementInterface
{
    /**
     * Get assigned labels
     *
     * @param $product
     * @return LabelInterface[]
     */
    public function getAssignedLabels($product);

    /**
     * Get assign label ids
     * @param $productId
     * @return array
     */
    public function getAssignedLabelsIds($productId);
    /**
     * Assign product to given labels
     *
     * @param int $productId
     * @param int[] $labelIds
     * @return bool
     */
    public function assignProductToLabels($productId, array $labelIds);
}
