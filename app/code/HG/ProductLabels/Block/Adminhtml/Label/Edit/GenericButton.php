<?php

namespace HG\ProductLabels\Block\Adminhtml\Label\Edit;

use Magento\Backend\Block\Widget\Context;
use HG\ProductLabels\Api\LabelRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var LabelRepositoryInterface
     */
    protected $labelRepository;

    /**
     * @param Context $context
     * @param LabelRepositoryInterface $labelRepository
     */
    public function __construct(
        Context $context,
        LabelRepositoryInterface $labelRepository
    )
    {
        $this->context = $context;
        $this->labelRepository = $labelRepository;
    }

    /**
     * Return label ID
     *
     * @return int|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getLabelId()
    {
        try {
            $id = $this->context->getRequest()->getParam('label_id');
            if ($id) {
                return $this->labelRepository->getById($id)->getId();
            }
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param string $route
     * @param array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
