<?php

namespace HG\ProductLabels\Block\Product;

use HG\ProductLabels\Api\LabelRepositoryInterface;
use HG\ProductLabels\Model\Config;
use Magento\Catalog\Model\Product;

class LabelList extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Product
     */
    protected $_product = null;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var LabelRepositoryInterface
     */
    private $labelRepository;

    /**
     * @var Config
     */
    private $_config;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param LabelRepositoryInterface $labelRepository
     * @param Config $_config
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        LabelRepositoryInterface $labelRepository,
        Config $_config,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->labelRepository = $labelRepository;
        $this->_config = $_config;
        parent::__construct($context, $data);
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = $this->_coreRegistry->registry('product');
        }
        return $this->_product;
    }

    /**
     * @return array|\HG\ProductLabels\Api\Data\LabelInterface[]
     */
    public function getProductLabels($product)
    {
        if($this->_config->isEnabled()) {
            return $this->labelRepository->getItemsByProduct($product);
        }
        return [];
    }
}
