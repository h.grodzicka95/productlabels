<?php

namespace HG\ProductLabels\Ui\Component\Label\Listing\Column;

use HG\ProductLabels\Model\Label;
use Magento\Framework\Data\OptionSourceInterface;
use HG\ProductLabels\Model\ResourceModel\Label\CollectionFactory;

class Options implements OptionSourceInterface
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var array
     */
    protected $options;

    /**
     * @var array
     */
    protected $currentOptions = [];

    /**
     * Options constructor.
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options !== null) {
            return $this->options;
        }
        $this->getOptions();
        return $this->options;
    }

    private function getOptions() {
        $labels = $this->collectionFactory->create();
        /**
         * @var $label Label
         */
        foreach ($labels as $label) {
            $this->options[] = [
                'label' => $label->getLabelText(),
                'value' => $label->getLabelId(),
            ];
        }
    }
}
