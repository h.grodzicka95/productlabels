<?php

namespace HG\ProductLabels\Ui\Component\Label\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class Actions extends Column
{
    const LABEL_URL_PATH_EDIT = 'hg_productlabels/label/edit';
    const LABEL_URL_PATH_DELETE = 'hg_productlabels/label/delete';

    /** @var UrlInterface */
    protected $_urlBuilder;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    )
    {
        $this->_urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['label_id'])) {
                    $labelId = $item['label_id'];
                    $item[$name]['edit'] = [
                        'href' => $this->_urlBuilder->getUrl(
                            self::LABEL_URL_PATH_EDIT, ['label_id' => $labelId]
                        ),
                        'label' => __('Edit')
                    ];
                    $item[$name]['delete'] = [
                        'href' => $this->_urlBuilder->getUrl(
                            self::LABEL_URL_PATH_DELETE, ['label_id' => $labelId]
                        ),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete %1', $item['label_text']),
                            'message' => __('Are you sure you want to delete this label?')
                        ],
                        'post' => true
                    ];
                }
            }
        }

        return $dataSource;
    }
}
