<?php

namespace HG\ProductLabels\Ui\DataProvider\Product\Form\Modifier;

use HG\ProductLabels\Model\LabelLinkManagement;
use HG\ProductLabels\Ui\Component\Label\Listing\Column\Options;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\MultiSelect;
use Magento\Ui\Component\Form\Field;

class Labels extends AbstractModifier
{
    const LABELS_INDEX = 'product_labels';
    const LABELS_FIELD_NAME = 'labels';

    /**
     * @var array
     */
    protected $meta = [];

    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @var Options
     */
    private $options;

    /**
     * @var LabelLinkManagement
     */
    private $labelLinkManagement;

    /**
     * @var \Magento\Framework\AuthorizationInterface
     */
    private $_authorization;

    /**
     * Labels constructor.
     * @param LocatorInterface $locator
     * @param Options $options
     * @param LabelLinkManagement $labelLinkManagement
     * @param \Magento\Framework\AuthorizationInterface $authorization
     */
    public function __construct(
        LocatorInterface $locator,
        Options $options,
        LabelLinkManagement $labelLinkManagement,
        \Magento\Framework\AuthorizationInterface $authorization
    ) {
        $this->locator = $locator;
        $this->options = $options;
        $this->labelLinkManagement = $labelLinkManagement;
        $this->_authorization = $authorization;
    }

    /**
     * @inheritdoc
     */
    public function modifyData(array $data)
    {
        $product   = $this->locator->getProduct();
        $productId = $product->getId();
        $data = array_replace_recursive(
            $data, [
            $productId => [
                'product' => [
                    self::LABELS_FIELD_NAME => $this->getProductLabelsValues(),
                ]
            ]
        ]);
        return $data;
    }

    private function getProductLabelsValues()
    {
        $id = $this->locator->getProduct()->getId();
        return $this->labelLinkManagement->getAssignedLabelsIds($id);
    }

    /**
     * @inheritdoc
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;
        $this->addLabelsFieldset();
        return $this->meta;
    }

    /**
     * @return void
     */
    protected function addLabelsFieldset()
    {
        $this->meta = array_merge_recursive(
            $this->meta,
            [
                static::LABELS_INDEX => $this->getFieldsetConfig(),
            ]
        );
    }

    /**
     * @return array
     */
    protected function getFieldsetConfig()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Assign Product Labels'),
                        'componentType' => Fieldset::NAME,
                        'dataScope' => static::DATA_SCOPE_PRODUCT,
                        'provider' => static::DATA_SCOPE_PRODUCT . '_data_source',
                        'ns' => static::FORM_NAME,
                        'collapsible' => true,
                        'sortOrder' => 10,
                        'opened' => true,
                    ],
                ],
            ],
            'children' => [
                self::LABELS_FIELD_NAME => $this->getMultiSelectFieldConfig(10),
            ],
        ];
    }

    /**
     * @param $sortOrder
     * @return array
     */
    protected function getMultiSelectFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Labels'),
                        'componentType' => Field::NAME,
                        'formElement' => MultiSelect::NAME,
                        'dataScope' => self::LABELS_FIELD_NAME,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                        'options' => $this->_getOptions(),
                        'visible' => true,
                        'disabled' => !$this->isAllowed(),
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    protected function _getOptions()
    {
        return $this->options->toOptionArray();
    }

    protected function isAllowed()
    {
        return $this->_authorization->isAllowed('HG_ProductLabels::label_save');
    }
}
