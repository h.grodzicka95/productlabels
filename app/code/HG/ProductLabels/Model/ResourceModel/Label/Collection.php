<?php

namespace HG\ProductLabels\Model\ResourceModel\Label;

use HG\ProductLabels\Model\Label;
use HG\ProductLabels\Model\ResourceModel\Label as LabelResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(Label::class, LabelResource::class);
    }
}
