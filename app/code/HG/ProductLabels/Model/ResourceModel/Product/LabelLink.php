<?php

namespace HG\ProductLabels\Model\ResourceModel\Product;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class LabelLink extends AbstractDb
{

    /**
     * Label products table name
     *
     * @var string
     */
    protected $_labelProductTable;

    protected function _construct()
    {
    }

    /**
     * Product labels table name getter
     *
     * @return string
     */
    public function getLabelProductTable()
    {
        if (!$this->_labelProductTable) {
            $this->_labelProductTable = $this->getTable('product_labels');
        }
        return $this->_labelProductTable;
    }

    /**
     * Get labels assigned to product
     *
     * @param  $product
     * @return array
     */
    public function getLabels($product)
    {
        $select = $this->getConnection()->select()->from(
            ['l' => 'labels']
        )->joinLeft(
            ['pl' => $this->getTable('product_labels')],
            'l.label_id=pl.label_id'
        )->where(
            'pl.product_id = ?',
            $product->getId()
        );

        $bind = ['pl.product_id' => $product->getId()];

        return $this->getConnection()->fetchAll($select, $bind);
    }

    /**
     * Get label ids assigned to product
     *
     * @param  $productId
     * @return array
     */
    public function getLabelIds($productId)
    {
        $select = $this->getConnection()->select()->from(
            'product_labels',
            ['label_id']
        )->where(
            "{$this->getTable('product_labels')}.product_id = ?",
            $productId
        );

        $bind = ['product_id' => $productId];

        return $this->getConnection()->fetchCol($select, $bind);
    }

    /**
     * Save product-label links
     * @param $productId
     * @param $labelIds
     * @return $this
     */
    public function saveLabelLinks($productId, $labelIds)
    {
        /**
         * Example re-save label
         */
        if ($labelIds === null) {
            return $this;
        }

        /**
         * old label-product relationships
         */
        $oldLabels = $this->getLabelIds($productId);
        $insert = array_diff($labelIds, $oldLabels);
        $delete = array_diff($oldLabels, $labelIds);

        $connection = $this->getConnection();

        /**
         * Delete labels from product
         */
        if (!empty($delete)) {
            $cond = ['label_id IN(?)' => $delete, 'product_id=?' => $productId];
            $connection->delete($this->getLabelProductTable(), $cond);
        }

        /**
         * Add labels to product
         */
        if (!empty($insert)) {
            $data = [];
            foreach ($insert as $labelId) {
                $data[] = [
                    'label_id' => (int)$labelId,
                    'product_id' => (int)$productId,
                ];
            }
            $connection->insertMultiple($this->getLabelProductTable(), $data);
        }

        return $this;
    }

}
