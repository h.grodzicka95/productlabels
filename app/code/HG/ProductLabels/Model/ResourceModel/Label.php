<?php

namespace HG\ProductLabels\Model\ResourceModel;

use HG\ProductLabels\Model\ResourceModel\Label\CollectionFactory;
use Magento\Catalog\Model\Product;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Label extends AbstractDb
{

    /**
     * Label products table name
     *
     * @var string
     */
    protected $_labelProductTable;

    /**
     * @var CollectionFactory
     */
    private $labelCollection;

    protected function _construct()
    {
        $this->_init('labels', 'label_id');
    }

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        CollectionFactory $collectionFactory,
        $connectionName = null
    )
    {
        $this->labelCollection = $collectionFactory;
        parent::__construct($context, $connectionName);
    }

    /**
     * Product labels table name getter
     *
     * @return string
     */
    public function getLabelProductTable()
    {
        if (!$this->_labelProductTable) {
            $this->_labelProductTable = $this->getTable('product_labels');
        }
        return $this->_labelProductTable;
    }

    /**
     * Get products assigned to label
     *
     * @param \HG\ProductLabels\Model\Label $label
     * @return array
     */
    public function getProducts($label)
    {
        $select = $this->getConnection()->select()->from(
            $this->getLabelProductTable(),
            ['product_id', 'label_id']
        )->where(
            "{$this->getTable('product_labels')}.label_id = ?",
            $label->getId()
        );

        $bind = ['label_id' => (int)$label->getId()];

        return $this->getConnection()->fetchPairs($select, $bind);
    }

    /**
     * Process label data after save
     *
     * Save related products ids
     *
     * @param \Magento\Framework\DataObject $object
     * @return $this
     */
    protected function _afterSave(\Magento\Framework\DataObject $object)
    {
        $this->_saveLabelProducts($object);
        return parent::_afterSave($object);
    }

    /**
     * Save label products relation
     *
     * @param \HG\ProductLabels\Model\Label $label
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _saveLabelProducts($label)
    {
        $label->setIsChangedProductList(false);
        $id = $label->getId();
        /**
         * new label-product relationships
         */
        $products = $label->getPostedProducts();

        /**
         * Example re-save label
         */
        if ($products === null) {
            return $this;
        }

        /**
         * old label-product relationships
         */
        $oldProducts = $label->getProducts();

        $insert = array_diff_key($products, $oldProducts);
        $delete = array_diff_key($oldProducts, $products);

        /**
         * Find product ids which are presented in both arrays
         * and saved before (check $oldProducts array)
         */
        $update = array_intersect_key($products, $oldProducts);
        $update = array_diff_assoc($update, $oldProducts);

        $connection = $this->getConnection();

        /**
         * Delete products from label
         */
        if (!empty($delete)) {
            $cond = ['product_id IN(?)' => array_keys($delete), 'label_id=?' => $id];
            $connection->delete($this->getLabelProductTable(), $cond);
        }

        /**
         * Add products to label
         */
        if (!empty($insert)) {
            $data = [];
            foreach ($insert as $productId => $position) {
                $data[] = [
                    'label_id' => (int)$id,
                    'product_id' => (int)$productId,
                ];
            }
            $connection->insertMultiple($this->getLabelProductTable(), $data);
        }

        if (!empty($insert) || !empty($delete)) {
            $productIds = array_unique(array_merge(array_keys($insert), array_keys($delete)));

            $label->setChangedProductIds($productIds);
        }

        if (!empty($insert) || !empty($update) || !empty($delete)) {
            $label->setIsChangedProductList(true);

            /**
             * Setting affected products to label for third party engine index refresh
             */
            $productIds = array_keys($insert + $delete + $update);
            $label->setAffectedProductIds($productIds);
        }
        return $this;
    }

    /**
     * @param Product $product
     */
    public function getLabelsByProduct($product)
    {
       $collection = $this->labelCollection->create();
        $collection->getSelect()
            ->join(
                ['pl' => $collection->getTable('product_labels')],
                'pl.label_id=main_table.label_id AND pl.product_id=' . $product->getId()
            );
        return $collection;
    }
}
