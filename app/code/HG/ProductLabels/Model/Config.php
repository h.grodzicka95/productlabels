<?php

declare(strict_types=1);

namespace HG\ProductLabels\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Config
{
    private const XML_PATH_HG_PRODUCT_LABELS_ENABLED = 'hgproductlabels/general/enabled';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Get is enabled
     *
     * @param string $scopeType
     * @param null|int|string $scopeCode
     * @return bool
     */
    public function isEnabled($scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeCode = null): bool
    {
        return !! $this->scopeConfig->getValue(
            self::XML_PATH_HG_PRODUCT_LABELS_ENABLED,
            $scopeType,
            $scopeCode
        );
    }
}
