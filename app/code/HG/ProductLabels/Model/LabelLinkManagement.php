<?php

namespace HG\ProductLabels\Model;

use HG\ProductLabels\Model\ResourceModel\Product\LabelLink;

class LabelLinkManagement implements \HG\ProductLabels\Api\Data\LabelLinkManagementInterface
{
    private LabelLink $productLabelLinkResource;

    /**
     * LabelLinkManagement constructor.
     * @param LabelLink $productLabelLinkResource
     */
    public function __construct(
        LabelLink $productLabelLinkResource
    ) {
        $this->productLabelLinkResource = $productLabelLinkResource;
    }

    public function getAssignedLabels($product)
    {
        return $this->productLabelLinkResource->getLabels($product);
    }

    public function getAssignedLabelsIds($productId)
    {
        $labelIds = $this->productLabelLinkResource->getLabelIds($productId);
        return array_map(function ($val) {
            return (int)$val;
        }, $labelIds);
    }

    public function assignProductToLabels($productId, array $labelIds)
    {
        $this->productLabelLinkResource->saveLabelLinks($productId, $labelIds);
    }
}
