<?php

namespace HG\ProductLabels\Model;

use HG\ProductLabels\Api\LabelRepositoryInterface;
use HG\ProductLabels\Api\Data;
use HG\ProductLabels\Model\ResourceModel\Label as ResourceLabel;
use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;

class LabelRepository implements LabelRepositoryInterface
{
    /**
     * @var ResourceLabel
     */
    protected $resource;

    /**
     * @var LabelFactory
     */
    protected $labelFactory;

    public function __construct(
        ResourceLabel $resource,
        LabelFactory $labelFactory
    )
    {
        $this->resource = $resource;
        $this->labelFactory = $labelFactory;
    }

    /**
     * Get label by ID
     * @param $labelId
     * @return Data\LabelInterface
     * @throws NoSuchEntityException
     */
    public function getById($labelId): Data\LabelInterface
    {
        $label = $this->labelFactory->create();
        $this->resource->load($label, $labelId);
        if (!$label->getId()) {
            throw new NoSuchEntityException(__('The label with the "%1" ID doesn\'t exist.', $labelId));
        }
        return $label;
    }

    /**
     * Save label
     * @param Data\LabelInterface $label
     * @return Data\LabelInterface
     * @throws CouldNotSaveException
     */
    public function save(Data\LabelInterface $label): Data\LabelInterface
    {
        try {
            $this->resource->save($label);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $label;
    }

    /**
     * Delete label
     * @param Data\LabelInterface $label
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Data\LabelInterface $label): bool
    {
        try {
            $this->resource->delete($label);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * Delete label by ID
     * @param $labelId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($labelId): bool
    {
        $label = $this->getById($labelId);
        return $this->delete($label);
    }

    /**
     * Get labels assigned to product
     * @param Product $product
     * @return mixed
     */
    public function getItemsByProduct($product)
    {
        return $this->resource->getLabelsByProduct($product);
    }
}
