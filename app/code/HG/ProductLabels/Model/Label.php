<?php

namespace HG\ProductLabels\Model;

use HG\ProductLabels\Api\Data\LabelInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use HG\ProductLabels\Model\ResourceModel\Label as LabelResource;

class Label extends AbstractModel implements LabelInterface,IdentityInterface
{
    const CACHE_TAG = 'hg_productlabels_label';

    protected $_cacheTag = 'hg_productlabels_label';

    protected $_eventPrefix = 'hg_productlabels_label';

    protected function _construct()
    {
        $this->_init(LabelResource::class);
    }

    public function getIdentities(): array
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues(): array
    {
        return [];
    }

    /**
     * Get label id
     * @return int|null
     */
    public function getLabelId(): ?int
    {
        return $this->getData(self::LABEL_ID);
    }

    /**
     * Get label text
     * @return string
     */
    public function getLabelText(): string
    {
        return $this->getData(self::LABEL_TEXT);
    }

    /**
     * Get label color
     * @return string
     */
    public function getLabelColor(): string
    {
        return $this->getData(self::LABEL_COLOR);
    }

    /**
     * Get created at
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Get updated at
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * Set label id
     * @param int $id
     * @return LabelInterface
     */
    public function setLabelId(int $id): LabelInterface
    {
        return $this->setData(self::LABEL_ID, $id);
    }

    /**
     * Set label text
     * @param string $labelText
     * @return LabelInterface
     */
    public function setLabelText(string $labelText): LabelInterface
    {
        return $this->setData(self::LABEL_TEXT, $labelText);
    }

    /**
     * Set label color
     * @param string $labelColor
     * @return LabelInterface
     */
    public function setLabelColor(string $labelColor): LabelInterface
    {
        return $this->setData(self::LABEL_COLOR, $labelColor);
    }

    /**
     * Set created at
     * @param string $createdAt
     * @return LabelInterface
     */
    public function setCreatedAt(string $createdAt): LabelInterface
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Set updated at
     * @param string $updatedAt
     * @return LabelInterface
     */
    public function setUpdatedAt(string $updatedAt): LabelInterface
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    public function getProductsReadonly() {
        return false;
    }

    public function getProducts()
    {
        if (!$this->getId()) {
            return [];
        }

        $array = $this->getData('products');
        if ($array === null) {
            $array = $this->getResource()->getProducts($this);
            $this->setData('products', $array);
        }
        return $array;
    }
}
