<?php

namespace HG\ProductLabels\Observer;

use HG\ProductLabels\Api\Data\LabelLinkManagementInterface;
use HG\ProductLabels\Model\LabelLinkManagement;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class SaveProductLabels implements ObserverInterface
{
    private RequestInterface $request;
    private LabelLinkManagementInterface $labelLinkManagement;

    public function __construct(RequestInterface $request, LabelLinkManagementInterface $labelLinkManagement)
    {
        $this->request = $request;
        $this->labelLinkManagement = $labelLinkManagement;
    }

    /**
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        /** @var  $product \Magento\Catalog\Model\Product */
        $product = $observer->getEvent()->getProduct();
        $productId = $product->getId();
        $params = $this->request->getParams();
        if (isset($params['product']) && isset($params['product']['labels'])) {
            $labelIds = !empty($params['product']['labels']) ? $params['product']['labels'] : [];
            $this->labelLinkManagement->assignProductToLabels($productId, $labelIds);
        }
        return $this;
    }
}
