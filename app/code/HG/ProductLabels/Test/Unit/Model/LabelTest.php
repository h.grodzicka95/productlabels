<?php

declare(strict_types=1);

namespace HG\ProductLabels\Test\Unit\Model;

use HG\ProductLabels\Model\Label;
use HG\ProductLabels\Model\ResourceModel\Label as LabelResource;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @covers \HG\ProductLabels\Model\Label
 */
class LabelTest extends TestCase
{
    /**
     * Testable Object
     *
     * @var Label
     */
    private $labelModel;

    /**
     * Object Manager
     *
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var ManagerInterface|MockObject
     */
    private $eventManagerMock;

    /**
     * @var Context|MockObject
     */
    private $contextMock;

    /**
     * @var LabelResource|MockObject
     */
    private $resourceMock;

    /**
     * Set Up
     *
     * @return void
     */
    protected function setUp(): void
    {
        $this->resourceMock = $this->createMock(LabelResource::class);
        $this->eventManagerMock = $this->getMockForAbstractClass(ManagerInterface::class);
        $this->contextMock = $this->createMock(Context::class);
        $this->contextMock->expects($this->any())->method('getEventDispatcher')->willReturn($this->eventManagerMock);
        $this->objectManager = new ObjectManager($this);
        $this->labelModel = $this->objectManager->getObject(
            Label::class,
            [
                'context' => $this->contextMock,
                'resource' => $this->resourceMock,
            ]
        );
    }

//    /**
//     * Test getIdentities method
//     *
//     * @return void
//     */
//    public function testGetIdentities()
//    {
//        $result = $this->labelModel->getIdentities();
//        $this->assertIsArray($result);
//    }
//
//    /**
//     * Test getId method
//     *
//     * @return void
//     */
//    public function testGetId()
//    {
//        $labelId = 1;
//        $this->labelModel->setData(Label::LABEL_ID, $labelId);
//        $expected = $labelId;
//        $actual = $this->labelModel->getLabelId();
//        self::assertEquals($expected, $actual);
//    }
//
//    /**
//     * Test getLabelText method
//     *
//     * @return void
//     */
//    public function testGetText()
//    {
//        $text = 'test02';
//        $this->labelModel->setData(Label::LABEL_TEXT, $text);
//        $expected = $text;
//        $actual = $this->labelModel->getTitle();
//        self::assertEquals($expected, $actual);
//    }
//
//    /**
//     * Test getLabelColor method
//     *
//     * @return void
//     */
//    public function testGetColor()
//    {
//        $color = 'test03';
//        $this->labelModel->setData(Label::LABEL_COLOR, $color);
//        $expected = $color;
//        $actual = $this->labelModel->getContent();
//        self::assertEquals($expected, $actual);
//    }
//
//    /**
//     * Test getCreatedAt method
//     *
//     * @return void
//     */
//    public function testGetCreatedAt()
//    {
//        $createdAt = 'test04';
//        $this->labelModel->setData(Label::CREATED_AT, $createdAt);
//        $expected = $createdAt;
//        $actual = $this->labelModel->getCreationTime();
//        self::assertEquals($expected, $actual);
//    }
//
//    /**
//     * Test getUpdatedAt method
//     *
//     * @return void
//     */
//    public function testGetUpdatedAt()
//    {
//        $updatedAt = 'test05';
//        $this->labelModel->setData(Label::UPDATED_AT, $updatedAt);
//        $expected = $updatedAt;
//        $actual = $this->labelModel->getUpdatedAt();
//        self::assertEquals($expected, $actual);
//    }
//
//    /**
//     * Test setId method
//     *
//     * @return void
//     */
//    public function testSetId()
//    {
//        $labelId = 15;
//        $this->labelModel->setLabelId($labelId);
//        $expected = $labelId;
//        $actual = $this->labelModel->getData(Label::LABEL_ID);
//        self::assertEquals($expected, $actual);
//    }
//
//
//    /**
//     * Test setLabelText method
//     *
//     * @return void
//     */
//    public function testSetText()
//    {
//        $text = 'test07';
//        $this->labelModel->setLabelText($text);
//        $expected = $text;
//        $actual = $this->labelModel->getData(Label::LABEL_TEXT);
//        self::assertEquals($expected, $actual);
//    }
//
//    /**
//     * Test setLabelColor method
//     *
//     * @return void
//     */
//    public function testSetColor()
//    {
//        $color = 'test08';
//        $this->labelModel->setLabelColor($color);
//        $expected = $color;
//        $actual = $this->labelModel->getData(Label::LABEL_COLOR);
//        self::assertEquals($expected, $actual);
//    }
//
//    /**
//     * Test setCreatedAt method
//     *
//     * @return void
//     */
//    public function testSetCreatedAt()
//    {
//        $createdAt = 'test09';
//        $this->labelModel->setCreatedAt($createdAt);
//        $expected = $createdAt;
//        $actual = $this->labelModel->getData(Label::CREATED_AT);
//        self::assertEquals($expected, $actual);
//    }
//
//    /**
//     * Test setUpdatedAt method
//     *
//     * @return void
//     */
//    public function testSetUpdatedAt()
//    {
//        $updatedAt = 'test10';
//        $this->labelModel->setUpdatedAt($updatedAt);
//        $expected = $updatedAt;
//        $actual = $this->labelModel->getData(Label::UPDATED_AT);
//        self::assertEquals($expected, $actual);
//    }
}
